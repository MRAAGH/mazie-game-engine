// This is free and unencumbered software released into the public domain.

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.

// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

// For more information, please refer to <https://unlicense.org>



// This is an example game.js file.
// You can use it as a starting point for your game.
// Alternatively, you can use generategame.py to generate this automatically.


function initializeGame() {
    // your custom initialization code goes here
    // The following example creates an additional overlay and
    // initializes an example data entry
    overlayNewImage('empty.png', 'something');
    data.something = 0;

    // This is your game's starting screen
    loadScreen('a1f.png');
    // if you do not specify the extension, like this:
    // loadScreen('a1f');
    // it will default to .webp
}

function customDisplayLogic() {
    // Your custom code to execute after each click.

    // The following example checks some data entry and changes an overlay
    // and also adds an additional clickable area to every screen
    if (data.something > 0) {
        setImage('something.png', 'something');
        addClickableArea({coords:'1,79,10,98', t:'a1r.png'});
    } else {
        setImage('empty.png', 'something');
    }
}

// This is where screens are defined and their logic.
const screens = {

    // each image in game/img/ should be specified here
    // unless it is an overlay
    'a1f.png': {
        areas: [
            {coords: '25,75,75,100', t:'a1n.png'}, // click bottom of screen, go to a1n
            {coords: '30,20,70,70',  t:'a2f.png'}, // click middle of screen, go to a2f
            {coords: '0,0,18,100',   t:'a1l.png'}, // click left   of screen, go to a1l
            {coords: '82,0,100,100', t:'a1r.png'}, // click right  of screen, go to a1r
        ]
    },

    'a1l.png': {
        areas: [
            {coords: '25,75,75,100', t:'a1r.png'},
            {coords: '0,0,18,100',   t:'a1n.png'},
            {coords: '82,0,100,100', t:'a1f.png'},
        ]
    },

    'a1r.png': {
        areas: [
            {coords: '25,75,75,100', t:'a1l.png'},
            {coords: '0,0,18,100',   t:'a1f.png'},
            {coords: '82,0,100,100', t:'a1n.png'},
        ]
    },

    'a1n.png': {
        areas: [
            {coords: '25,75,75,100', t:'a1f.png'},
            {coords: '0,0,18,100',   t:'a1r.png'},
            {coords: '82,0,100,100', t:'a1l.png'},
        ]
    },

    // a more advanced example, showcasing all the features:
    'a2f.png': {
        areas: [
            // the format for coordinates is: 'x1,y1,x2,y2'
            {coords: '30,30,35,70', t:'a1f.png'},
            // you can specify cursor manually:
            {coords: '40,30,45,70', cursor: 'heart.png', t:'a1f.png'},
            // you can specify href:
            {coords: '50,30,55,70', href: 'https://mazie.rocks'},
            // you can play sound when clicked:
            {coords: '60,30,65,70', playsound: 'meow.ogg'},
            // you can specify an event:
            {coords: '70,30,75,70', event: ()=>data.something+=1},
            // you can specify a condition:
            {coords: '80,30,85,70', t:'a1f.png', cond: ()=>data.something>0},
        ],
        // you can specify some alternative images with conditions:
        alt: {
            'a2f_alt1.png': ()=>data.something===1,
            'a2f_alt2.png': ()=>data.something===2,
        },
        // you can specify new music which will start looping here:
        setmusic: 'bird.ogg',
        // you can specify a preprocessing function
        preprocess: ()=>{
            if (data.something > 2) {
                data.something = 0;
            }
        },
    },

};
