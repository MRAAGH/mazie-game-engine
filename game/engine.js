// This is free and unencumbered software released into the public domain.

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.

// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

// For more information, please refer to <https://unlicense.org>


// This is the main game engine file.
// You should not need to edit this in order to use the game engine.

let htmlImage;
let htmlMap;
let htmlDebug;
let htmlDevmask;
let devmodeEnabled = false;
let currentScreenName = '';
let currentMusicName = undefined;
let activeAreaOrder = [];
let rememberedDevClick = null;
const audioCollection = {};
const data = {};

document.addEventListener('DOMContentLoaded', function() {
    htmlImage = document.getElementById('image1');
    htmlMap = document.getElementById('map');
    htmlDebug = document.getElementById('debug');
    initializeGame();
    htmlDevmask = overlayNewImage('devmask.png', 'devmask');
    htmlImage.addEventListener('mousedown', e => handleGlobalMousedown(e));
    devmodeDisable();
    preloadAllAssets();
    window.addEventListener('resize', onResize, false);
    window.addEventListener('keydown', onKeydown, false);
});

function onResize() {
    // To force the <area>s to be recalculated to the new size,
    // reload the current screen.
    loadScreen(currentScreenName);
}

function onKeydown(e) {
    if (e.keyCode === 115) {
        devmodeToggle();
    }
}

function devmodeToggle() {
    if (devmodeEnabled) {
        devmodeDisable();
    } else {
        devmodeEnable();
    }
    devmodeEnabled = !devmodeEnabled;
}

function devmodeEnable() {
    htmlDebug.style.display = 'block';
    htmlDevmask.style.display = 'block';
}

function devmodeDisable() {
    htmlDebug.style.display = 'none';
    htmlDevmask.style.display = 'none';
}

// Add another image layer (an <image> DOM element)
// This should only be done during initialization,
// and later, the element can be accessed by ID
function overlayNewImage(src, id) {
    const htmlNewImage = new Image();
    htmlNewImage.src = resolveImageFile(src);
    htmlNewImage.id = id;
    htmlNewImage.ondragstart = () => false;
    htmlNewImage.setAttribute('usemap', '#map');
    document.body.insertBefore(htmlNewImage, htmlMap);
    htmlNewImage.addEventListener('mousedown', e => handleGlobalMousedown(e));
    return htmlNewImage;
}

// Change the source file of an existing image
function setImage(src, id) {
    document.getElementById(id).src = resolveImageFile(src);
}

// Convert area from config format to a DOM element.
// Returns null if area has an unfulfilled condition.
function areaToHtml(area, areaId) {
    if (area.cond && !area.cond()) return null;
    const htmlArea = document.createElement('area');
    htmlArea.shape = 'rect';
    htmlArea.coords = toScreenCoords(area.coords);
    if (area.cursor) {
        const cursorFile = resolveCusorFile(area.cursor);
        htmlArea.style.cursor = 'url('+cursorFile+') 16 16, pointer';
    } else {
        htmlArea.style.cursor = 'pointer';
    }
    if (area.href) {
        htmlArea.href = area.href;
    } else {
        htmlArea.addEventListener('mousedown', () => areaClicked(areaId));
    }
    return htmlArea;
}

// Add another clickable area on top of all others
function addClickableArea(area) {
    const areaId = activeAreaOrder.length;
    const htmlArea = areaToHtml(area, areaId);
    if (!htmlArea) return;
    htmlMap.insertBefore(htmlArea, htmlMap.firstChild);
    activeAreaOrder[areaId] = area;
}

// Add .webp if there is no file extension
function resolveImageFile(imageName) {
    return 'img/' + addExtensionIfMissing(imageName, '.webp');
}

// Add .png if there is no file extension
function resolveCusorFile(cursorName) {
    return 'cursor/' + addExtensionIfMissing(cursorName, '.png');
}

function addExtensionIfMissing(fileName, extension) {
    if (fileName.includes('.')) {
        return fileName;
    }
    return fileName+extension;
}

// Preload all image and audio assets, so there
// won't be a network delay later when they are needed
function preloadAllAssets() {
    htmlDebug.innerHTML = 'loading';
    // Find all unique cursors
    const allCursors = {};
    for (const screenName in screens) {
        const screen = screens[screenName];
        for (const area of screen.areas) {
            allCursors[area.cursor] = true;
        }
    }
    // Preload all cursors
    for (const cursorName in allCursors) {
        preloadImageByFileName(resolveCusorFile(cursorName));
    }
    for (const screenName in screens) {
        const screen = screens[screenName];
        // Preload the main image of this screen
        preloadImageByFileName(resolveImageFile(screenName));
        if (screen.alt) {
            for (const altName in screen.alt) {
                // Preload this alternative image
                preloadImageByFileName(resolveImageFile(altName));
            }
        }
        if (screen.setmusic) {
            // Preload the music for this screen
            preloadAudioByFname(screen.setmusic);
        }
        for (const area of screen.areas) {
            if (area.playsound) {
                // Preload the sound effect
                preloadAudioByFname(area.playsound);
            }
        }
    }
    htmlDebug.innerHTML = 'loaded';
}

// Preload an image asset, so there
// won't be a network delay later when it is needed
function preloadImageByFileName(fileName) {
    const newHtmlImage = new Image();
    newHtmlImage.src = fileName;
    // Insert at the start of body.
    // This avoids ugly visual image layering during initial loading
    document.body.insertBefore(newHtmlImage, document.body.firstChild);
}

// Preload an audio asset, so there
// won't be a network delay later when it is needed
function preloadAudioByFname(audioFname) {
    // skip if already exists
    if (audioCollection[audioFname]) return;
    audioCollection[audioFname] = new Audio('audio/'+audioFname);
}

function loadScreen(targetScreenName) {
    currentScreenName = targetScreenName;
    if (typeof screens[targetScreenName] === 'undefined') {
        console.error('Missing screen: ', targetScreenName);
        htmlDebug.innerHTML = targetScreenName+' (missing screen)';
        return;
    }
    htmlDebug.innerHTML = targetScreenName;
    activeAreaOrder = [];

    const currentScreen = screens[targetScreenName];

    if (currentScreen.preprocess) {
        currentScreen.preprocess();
    }

    const htmlAreas = [];
    for (let i = 0; i < currentScreen.areas.length; i++) {
        const area = currentScreen.areas[i];
        const htmlArea = areaToHtml(area, i);
        if (!htmlArea) continue;
        htmlAreas.push(htmlArea);
        activeAreaOrder[i] = area;
    }
    htmlMap.replaceChildren(...htmlAreas);

    // check if any of the alternative images should be displayed
    let imageName = targetScreenName;
    if (currentScreen.alt) {
        for (alt in currentScreen.alt) {
            if (currentScreen.alt[alt]()){
                imageName = alt;
                break;
            }
        }
    }
    // load new image
    htmlImage.src = resolveImageFile(imageName);

    // Music change
    if (currentScreen.setmusic) {
        setMusic(currentScreen.setmusic);
    }

    customDisplayLogic();
}

// Convert coordinates from range 0-100 to screen coordinates
function toScreenCoords(coords100Str) {
    // Scale factor for coordinates
    const scalex = htmlImage.width / 100;
    const scaley = htmlImage.height / 100;

    // Scale all coordinates by how much the image is scaled
    const coords100 = coords100Str.split(',');
    const coordsScaled = [
        coords100[0] * scalex,
        coords100[1] * scaley,
        coords100[2] * scalex,
        coords100[3] * scaley,
    ];
    return coordsScaled.join(',');
}

// Called from html <area> when clicked
function areaClicked(areaId) {
    const area = activeAreaOrder[areaId];
    if (!area) {
        console.error('Invalid area ID:', areaId);
        htmlDebug.innerHTML = 'Invalid area ID: ' + areaId;
        return;
    }

    if (area.event) {
        area.event();
    }

    if (area.playsound) {
        playSound(area.playsound);
    }

    if (area.t) {
        // load target screen
        loadScreen(area.t);
    } else {
        // reload the current screen (maybe something changed)
        loadScreen(currentScreenName);
    }
}

// development functionality.
// save click coordinates to clipboard
function handleGlobalMousedown(e) {
    // Ignore left and right mouse button
    if (e.buttons < 4) return;
    // ok, is middle mouse button
    // Scale factor for coordinates
    const scalex = htmlImage.width / 100;
    const scaley = htmlImage.height / 100;
    const x = Math.round(e.clientX / scalex);
    const y = Math.round(e.clientY / scaley);
    if (rememberedDevClick === null) {
        // is first click
        rememberedDevClick = { x:x, y:y };
        htmlDebug.innerHTML = '';
        return;
    }
    // is second click
    const x1 = Math.min(x, rememberedDevClick.x);
    const y1 = Math.min(y, rememberedDevClick.y);
    const x2 = Math.max(x, rememberedDevClick.x);
    const y2 = Math.max(y, rememberedDevClick.y);
    const str = x1+','+y1+','+x2+','+y2;
    rememberedDevClick = null;
    console.log(str);
    if (typeof navigator.clipboard === 'undefined') {
        htmlDebug.innerHTML = 'clipboard unavailable';
        return;
    }
    navigator.clipboard.writeText(str)
    .then(() => {
        htmlDebug.innerHTML = 'copied to clipboard';
    }, () => {
        htmlDebug.innerHTML = 'failed to copy';
    });
}

// Play a sound effect (restart if already playing)
function playSound(soundName) {
    // Play previously prepared Audio object
    // (this way we avoid re-downloading the audio)
    audioCollection[soundName].currentTime = 0;
    audioCollection[soundName].play();
}

// Stop previous music and play new music instead
function setMusic(newMusicName) {
    if (currentMusicName === newMusicName) {
        // no need to change music
        return;
    }
    if (currentMusicName) {
        // pause the existing music
        audioCollection[currentMusicName].pause();
    }
    currentMusicName = newMusicName;
    // Start from beginning
    audioCollection[newMusicName].currentTime = 0;
    // Start playing
    audioCollection[newMusicName].play();
    // Enable looping
    audioCollection[newMusicName].loop = true;
}
