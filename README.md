# Mazie Game Engine

A minimal game engine for simple web point-and-click adventure games, such as https://find.mazie.rocks

Available game features:

- custom event on click
- custom event when displaying screen
- link to external website
- custom mouse cursors
- custom overlays (inventory etc.)
- looping music
- different music for different areas
- sound effects
- progressive/optimized loading

Unsupported features:

- animations
- animated transitions
- anything timing-based
- non-static overlays (everything must have a fixed position on screen)

# How to make a game in this game engine

Some Javascript knowledge is required.

### Step 1: download the engine

Download all files in this repository.

You can test my example game by opening `game/index.html` using your web browser.

### Step 2: take pictures, record sounds

Acquire all your game assets that you want to use in your point-and-click adventure

Images go into `game/img/`, audio into `game/audio/`, mouse cursor images into `game/cursor/`.

### Step 3: game.js

In `game/game.js`, you need to specify screens and their clickable areas.
What is a screen? Each image is a screen (unless it's an overlay). Look at examples.

You should write your custom game logic for inventory/overlays etc. in `initializegame()` and `customdisplaylogic()`.

# Optional: my automation scripts

I have a few scripts that allow me to work more efficiently. Feel free to try these tools.

### Generate game.js automatically

If you want, you can generate `game/game.js` automatically using my code generator.

To do this, your images in `game/img/` have to be named correctly, so the generator knows how they relate to each other.

At the beginning of the file `generategame.py` there are instructions for how images should be named.

Read the generated game.js file. Everywhere where it says TODO, you need to do something manually. And you need to code all your custom game logic. Try to keep it all in the dedicated places: `initializegame()`, `customdisplaylogic()` and `screens`

### Preprocess photos/images

Put your .jpg images in the directory `original` (not inside `game`)

Run `convert.sh`. Your images will be scaled to 360px, converted to .webp and saved into `game/img/`. Edit the script to change the image size.

### Preprocess audio

Put your .wav files in the directory `originalaudio`.

Run `ffmpeg.sh`. Files will be converted to .ogg, volume adjusted, and saved into `game/audio/`. Edit the script to change the volume level.

# Additional tips

- `engine.js` should not need to be edited.
- `index.html` needs to be edited a little bit to change the webpage title and the initial image.
- pressing F4 in-game will enable dev overlay.
- if you middle-click twice, bounding box coordinates will be copied to clipboard in correct format.
