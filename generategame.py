#!/usr/bin/python3

# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <https://unlicense.org>


# -------------------------------------------------------
#  _           _                   _   _
# (_)_ __  ___| |_ _ __ _   _  ___| |_(_) ___  _ __  ___
# | | '_ \/ __| __| '__| | | |/ __| __| |/ _ \| '_ \/ __|
# | | | | \__ \ |_| |  | |_| | (__| |_| | (_) | | | \__ \
# |_|_| |_|___/\__|_|   \__,_|\___|\__|_|\___/|_| |_|___/
# -------------------------------------------------------

# This script generates initial Javascript code for your game
# based on the image names in the directory game/img/

# The images should be organized like this:
#   Images on main path:
#       either in groups of 4 (a1f, a1l, a1r, a1n),
#       or in groups of 2 (a1f, a1n).
#       In this example, 'a' is the letter of the section.
#       '1' is the sequential number within the section.
#       And f=forward, l=left, r=right, n=back is the look direction.
#   Images offpath (a branch from the main path):
#       either in groups of 4 (b5r1f, b5r1l, b5r1r, b5r1n)
#       or in groups of 2 (b5r1f, b5r1n)
#       or solo without specified direction (b5r1)
#       basically you repeat the name of the image which
#       you're branching from, and add a suffix.
#       In this example, we are branching from b5r.
#       Putting '1' as 4th character, means it is the 1st step off the path.
#       The 5th character (optional) is the relative look direction.
#   Up and down:
#       Any image can have an up and/or down view,
#       by appending u and/or d
#       for example if you have a1l you can make a1lu and/or a1ld
#       for example if you have b5r2f you can make b5r2fu and/or b5r1fd

# EXAMPLE IMAGE NAMING
#   a1f.webp    (first position on path 'a', facing forward)
#   a1l.webp    (first position on path 'a', facing left)
#   a1n.webp    (first position on path 'a', facing back)
#   a1r.webp    (first position on path 'a', facing right)
#   a2f.webp    (second position on path 'a', facing forward)
#   a2l.webp    (second position on path 'a', facing left)
#   a2n.webp    (second position on path 'a', facing back)
#   a2r.webp    (second position on path 'a', facing right)
#   a2r1.webp   (if you are at a2r.webp and go forward off the path)
#   a2r2.webp   (two steps off the path)
#   a2r3.webp   (three steps off the path)
#   a2r3u.webp  (if you're at a2r3.webp and look up)


# VISUALIZATION OF THIS EXAMPLE:

#     a2f                 a2r3u
#  a2l   a2r  a2r1  a2r2  a2r3
#     a2n
#
#     a1f
#  a1l   a1r
#     a1n


# This is everything that my generator supports.
# Deviating from my standard won't prevent you from making a game,
# but it will increase the manual work you need to do,
# because the code won't be generated automatically.

# In any case, the generated code is only a starting point.
# You still need to add all custom logic, such as displaying alternatives,
# conditional buttons, action buttons, enter/exit actions, and audio



# Here you can define how your sections are connected
# and the connections will be generated automagically.
# There are already 2 examples given.
# These examples will make it so:
#   - if you are at a4l and go forward, you will end up at b1f
#   - if you are at b1n and go forward, you will end up at a4r
#   - if you are at b10l and go forward, you will end up at c1r
#   - if you are at c1l and go forward, you will end up at b10r
YOUR_SECTION_CONNECTIONS = [
        # ('a4l', 'b1f'),
        # ('b10l', 'c1r'),
        ]


import os
import re

BACK = {
        'f': 'n',
        'l': 'r',
        'n': 'f',
        'r': 'l',
        }

LEFT = {
        'f': 'l',
        'l': 'n',
        'n': 'r',
        'r': 'f',
        }

RIGHT = {
        'f': 'r',
        'l': 'f',
        'n': 'l',
        'r': 'n',
        }

for i in range(len(YOUR_SECTION_CONNECTIONS)):
    (src, dst) = YOUR_SECTION_CONNECTIONS[i]
    # add the inverse section connection as well:
    src2 = dst[:-1] + BACK[dst[-1]]
    dst2 = src[:-1] + BACK[src[-1]]
    YOUR_SECTION_CONNECTIONS.append((src2, dst2))


imagefiles = os.listdir('game/img')

webpfiles = [f.split('.')[0] for f in imagefiles if f.split('.')[-1] == 'webp']

screennames = webpfiles

screennames.sort()

print("""//               'x1,y1,x2,y2'
const pathleft = {coords: '0,0,18,100', cursor: 'left'};
const pathforw = {coords: '30,20,70,70', cursor: 'forw'};
const pathforwupstairs = {coords: '30,0,70,50', cursor: 'forw'};
const pathforwdownstairs = {coords: '30,50,70,100', cursor: 'forw'};
const pathrigh = {coords: '82,0,100,100', cursor: 'right'};
const pathturn = {coords: '25,75,75,100', cursor: 'turn'};
const pathback = {coords: '25,75,75,100', cursor: 'back'};
const pathdown = {coords: '25,75,75,100', cursor: 'down'};
const left180d = {coords: '0,0,18,100', cursor: 'turn'};
const righ180d = {coords: '82,0,100,100', cursor: 'turn'};
const lookup   = {coords: '30,0,70,25', cursor: 'up'};
const lookupabovepath = {coords: '30,0,70,18', cursor: 'up'};

function initializeGame() {
    // your custom initialization code goes here
    // The following example creates an additional overlay and
    // initializes an example data entry
    /*
    overlayNewImage('img/example0.png', 'examplelayer');
    data.example_data_entry = 0;
    */

// TODO: verify your game's starting point:
    loadScreen('a1f');

}

function customDisplayLogic() {
    // Your custom code to execute after each click here.

    // The following example checks some data entry and changes an overlay
    // and also adds an additional clickable area on every screen
    /*
    if (data.example_data_entry > 0) {
        setImage('example2.png', 'examplelayer');
        addClickableArea({coords:'89,81,98,100', t:'somescreen'});
    }
    */
}

const screens = {""")

def printstart(m):
    print("    '{}': {{areas:[".format(m))

def printarea(preset, target):
    print("        {{...{}, t:'{}'}},".format(preset, target))

def printend():
    print("    ]},")


for m in screennames:

    # if suffix is u, this is up
    if m[-1] == 'u':
        printstart(m)
        # because this is up, the only button is down (back)
        notup = m[:-1]
        somethingnotup = notup in screennames
        if not somethingnotup:
            raise Exception("{} requires {} to exist"
                .format(m, notup))
        printarea('pathdown', notup)
        printend()
        continue

    # if suffix is d, this is down
    if m[-1] == 'd':
        printstart(m)
        # because this is down, the only button is up
        notdown = m[:-1]
        somethingnotdown = notdown in screennames
        if not somethingnotdown:
            raise Exception("{} requires {} to exist"
                .format(m, notdown))
        printarea('lookup', notdown)
        printend()
        continue

    # if suffix u exists, there is something up
    up = m+'u'
    somethingup = up in screennames
    # if suffix d exists, there is something down
    down = m+'d'
    somethingdown = down in screennames

    # check if it is on the main path:
    mainpathmatch = re.match('^([a-z])(\d+)([flnr])$', m)
    if mainpathmatch:
        # it is of form  a1f, a2l, b3n, d11f, ...
        section = mainpathmatch[1]
        num = int(mainpathmatch[2])
        direction = mainpathmatch[3]

        printstart(m)

        # determine if there is anything needing a forward button
        offpath1 = section+str(num)+direction+'1'
        offpath1f = offpath1+'f'
        somethingoffpath1 = offpath1 in screennames
        somethingoffpath1f = offpath1f in screennames
        somethingoffpath = somethingoffpath1 or somethingoffpath1f
        # only one type of offpath allowed:
        if somethingoffpath1 and somethingoffpath1f:
            raise Exception("Not allowed: {} and {} both exist"
                .format(somethingoffpath1, somethingoffpath1f))
        somethingpathforw = False
        if direction == 'f':
            pathforw = section+str(num+1)+direction
            somethingpathforw = pathforw in screennames
        if direction == 'n':
            pathforw = section+str(num-1)+direction
            somethingpathforw = pathforw in screennames
        sectionconnection = [con for con in YOUR_SECTION_CONNECTIONS if con[0] == m]
        somethingsectionconnection = len(sectionconnection) == 1
        if len(sectionconnection) > 1:
            raise Exception("Multiple section connections defined: {} (hint: you should only define each connection in one direction. The other direction is added automatically)"
                .format(sectionconnection))

        somethingforward = somethingpathforw or somethingoffpath or somethingsectionconnection
        # now make up button, if applicable
        if somethingup:
            if somethingforward:
                # something is forward, so up button will be small
                printarea('lookupabovepath', up)
            else:
                # nothing is forward, so up button can be big
                printarea('lookup', up)

        # now make forward button, if applicable
        if somethingforward:
            # path forward takes highest priority
            if somethingpathforw:
                # there is a path forward:
                printarea('pathforw', pathforw)
            elif somethingsectionconnection:
                printarea('pathforw', sectionconnection[0][1])
            elif somethingoffpath1:
                printarea('pathforw', offpath1)
            elif somethingoffpath1f:
                printarea('pathforw', offpath1f)



        # on main path, back must always exist
        back = section+str(num)+BACK[direction]
        somethingback = back in screennames
        if not somethingback:
            raise Exception("Missing picture {} (to be back from {} on path)"
                .format(back, m))

        # now make down/back button
        if somethingdown:
            # override back button with down button:
            printarea('pathdown', down)
        else:
            # make the back button normally:
            printarea('pathturn', back)

        # check if left and right exist
        left = section+str(num)+LEFT[direction]
        right = section+str(num)+RIGHT[direction]
        somethingleft = left in screennames
        somethingright = right in screennames

        # both must exist, or neither.
        if somethingleft != somethingright:
            raise Exception("{} and {} must both exist, or neither"
                .format(left, right))

        # make left button
        if somethingleft:
            # left button to turn left
            printarea('pathleft', left)
        else:
            # left button to turn around
            printarea('left180d', back)

        # make right button
        if somethingright:
            # right button to turn right
            printarea('pathrigh', right)
        else:
            # right button to turn around
            printarea('righ180d', back)

        printend()
        continue

    # check if it is on the main path:
    offpathmatch = re.match('^([a-z]\d+)([flnr])(\d+)([flnr]?)$', m)
    if offpathmatch:
        # it is of form  a1f1, a2l1f, b3n3l, d11f1n, ...
        parentpath = offpathmatch[1]
        direction = offpathmatch[2]
        offnum = int(offpathmatch[3])
        offdirection = offpathmatch[4]

        printstart(m)

        # determine if there is anything needing a forward button
        somethingforward = False
        if offdirection in ['', 'f']:
            nextoff1 = parentpath+direction+str(offnum+1)
            nextoff1f = nextoff1+'f'
            somethingnextoff1 = nextoff1 in screennames
            somethingnextoff1f = nextoff1f in screennames
            somethingforward = True
            if somethingnextoff1:
                forward = nextoff1
            elif somethingnextoff1f:
                forward = nextoff1f
            else:
                somethingforward = False
        if offdirection == 'n':
            if offnum == 1:
                # forward brings you back on path
                forward = parentpath+BACK[direction]
                somethingforward = forward in screennames
                # must exist
                if not somethingforward:
                    raise Exception("{} requires {} to return to path"
                        .format(m, forward))
            else:
                # forward brings you to previous offpath
                forward = parentpath+direction+str(offnum-1)+'n'
                somethingforward = forward in screennames
                # must exist
                if not somethingforward:
                    raise Exception("{} requires {} to return to path"
                        .format(m, forward))

        # now make up button, if applicable
        if somethingup:
            if somethingforward:
                # something is forward, so up button will be small
                printarea('lookupabovepath', up)
            else:
                # nothing is forward, so up button can be big
                printarea('lookup', up)

        # now make forward button, if applicable
        if somethingforward:
            printarea('pathforw', forward)

        # decide what is back
        if offdirection == '':
            isbackturn = False;
            # there are no directions on this offpath,
            # so back brings you to previous
            if offnum == 1:
                # going back puts you back on path
                back = parentpath+direction
            else:
                # going back puts you on previous offpath
                back1 = parentpath+direction+str(offnum-1)
                back1f = back1+'f'
                somethingback1 = back1 in screennames
                somethingback1f = back1f in screennames
                if somethingback1:
                    back = back1
                elif somethingback1f:
                    back = back1f
                else:
                    raise Exception("Need {} or {} to go back to from {}"
                        .format(back1, back1f, m))
                somethingback = True
            # also, because we need the back button, we can't have down
            if somethingdown:
                raise Exception("Can't have {} on {}, back button needed"
                    .format(down, m))
        else:
            isbackturn = True;
            # there are directions on the offpath (one of f,l,r,n)
            # so the back button should turn around.
            # Loking down is also allowed here, if down image exists.
            back = parentpath+direction+str(offnum)+BACK[offdirection]
            somethingback = back in screennames
            if not somethingback:
                raise Exception("{} demands {} to exist too"
                    .format(m, back))

        # now make down/back button
        if somethingdown:
            # override back button with down button:
            printarea('pathdown', down)
        else:
            # make the back button normally:
            printarea('pathturn' if isbackturn else 'pathback', back)

        # do left and right buttons
        if offdirection == '':
            # there are no directions on this offpath.
            # But if this is the first offpath screen,
            # left and right can bring you back on main path.
            if offnum == 1:
                left = parentpath+LEFT[direction]
                somethingleft = left in screennames
                if somethingleft:
                    print('// TODO: verify if this is what you want:')
                    printarea('pathleft', left)
                right = parentpath+RIGHT[direction]
                somethingright = right in screennames
                if somethingright:
                    print('// TODO: verify if this is what you want:')
                    printarea('pathrigh', right)
        else:
            # check if left and right exist
            left = parentpath+direction+str(offnum)+LEFT[offdirection]
            right = parentpath+direction+str(offnum)+RIGHT[offdirection]
            somethingleft = left in screennames
            somethingright = right in screennames

            # both must exist, or neither.
            if somethingleft != somethingright:
                raise Exception("{} and {} must both exist, or neither"
                    .format(somethingleft, somethingright))

            # make left button
            if somethingleft:
                # left button to turn left
                printarea('pathleft', left)
            else:
                # left button to turn around
                printarea('left180d', back)

            # make right button
            if somethingright:
                # right button to turn right
                printarea('pathrigh', right)
            else:
                # right button to turn around
                printarea('righ180d', back)

        printend()
        continue

    print("// TODO: '{}' not recognized. No code generated.".format(m))

print("};");



# Below: my old notes when I was planning this script.
# It's sad that the script is not as simple as these notes.
# Maybe if I used fewer variables it could be so simple.


# a3f
# if a3fu exist: up
# if a3fd exist: down else a3n down
# if a3+1f exist: forward else if a3f1 exist: forward
# if a3l exist: left, else a3n left
# if a3r exist: right, else a3n right

# a3n
# if a3nu exist: up
# if a3nd exist: down else a3f down
# if a3+1n exist: forward else if a3n1 exist: forward
# if a3r exist: left, else a3n left
# if a3l exist: right, else a3n right

# a3l
# if a3lu exist: up
# if a3ld exist: down else a3r down
# if a3l1 exist: forward
# a3n left
# a3f right

# a3r
# if a3ru exist: up
# if a3rd exist: down else a3l down
# if a3r1 exist: forward
# a3f left
# a3n right

# a3:4
# a3:4-1 back
# if a3:4+1 exist: forward
# if 4=1 and not a3:4+1 exist, left and right same as a3: [ADD COMMENT FOR REVIEW]

# a3:4f
# if a3:4fu exist: up
# if a3:4fd exist: down else a3:4n down
# if a3:4+1f exist: forward
# if a3:4l exist: left, else if a3:4n exist left
# if a3:4r exist: right, else if a3:4n exist right

# a3:4l

# a3:4r

# a3:4n
