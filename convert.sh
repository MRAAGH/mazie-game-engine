#!/bin/bash

# This script will pre-process all image assets.
# The original images should be in the directory original
# The resulting images will be stored in game/img

# It converts jpg to webp, and changes the resolution.

cd original
mkdir ../game
mkdir ../game/img
for file in $(find *); do convert $file -adaptive-resize 360x ../game/img/${file/jpg/webp}; done
