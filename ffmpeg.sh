#!/bin/bash

# This script will pre-process all audio assets.
# The original audio should be in the directory originalaudio
# The resulting files will be stored in game/audio

# It converts wav to ogg, and increases volume a bit.

cd originalaudio
mkdir ../game
mkdir ../game/audio
for file in $(find *); do ffmpeg -i "$file" -af volume=15dB ../game/audio/${file/wav/ogg} -y; done
